/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.doxygen

import org.gradle.testkit.runner.GradleRunner
import org.ysb33r.gradle.doxygen.internal.DoxygenDistributionDownloader
import org.ysb33r.gradle.doxygen.testfixtures.DownloadTestSpecification
import spock.lang.IgnoreIf
import spock.lang.TempDir

import java.nio.file.Files

@IgnoreIf({ DownloadTestSpecification.SKIP_TESTS || !DoxygenDistributionDownloader.DOWNLOAD_SUPPORTED })
class DoxygenTaskSpec extends DownloadTestSpecification {

    public static final File TESTFSREADROOT = new File(System.getProperty('TESTFSREADROOT') ?: 'src/downloadTest/resources')
    public static final File DOXY_TEMPLATE = new File(System.getProperty('DOXY_TEMPLATE') ?: 'src/main/resources/doxyfile-template.dox')

    @TempDir
    File testProjectDir

    File buildFile
    File projectDir
    File buildDir
    File settingsFile
    File testkitDir
    File sourceDir

    void setup() {
        projectDir = new File(testProjectDir, 'test-project')
        testkitDir = new File(testProjectDir, 'testkit')
        buildFile = new File(projectDir, 'build.gradle')
        buildDir = new File(projectDir, 'build')
        settingsFile = new File(projectDir, 'settings.gradle')
        sourceDir = new File(projectDir, 'src/main/cpp')
        projectDir.mkdirs()
        testkitDir.mkdirs()

        settingsFile.text = ''

        buildFile.text = """
        plugins {
            id 'org.ysb33r.doxygen'
        }
        
        doxygen {
          sourceSets {
            main {
                outputDir = 'build/docs'
            
                options generate_xml: false,
                    generate_latex: false,
                    generate_html: true,
                    have_dot: false,
                    aliases: 'ChangeLog=\\\\xrefitem ChangeLogs "ChangeLog" "ChangeLogs" '
            }
          }
        }
        """.stripIndent()

        sourceDir.mkdirs()
        Files.copy(
            new File(TESTFSREADROOT, 'sample-cpp/sample.cpp').toPath(),
            new File(sourceDir, 'sample.cpp').toPath()
        )
    }

    void "Run Doxygen to generate simple documentation with a default template"() {
        when:
        final result = gradleRunner.build()

        then:
        new File(buildDir, 'docs/html').exists()
        new File(buildDir, 'docs/html/index.html').exists()

        and:
        !result.output.matches(~/.*(warning: Invalid value)|(error: Invalid).*/)
    }

    void "Plugin is configuration cache compatible"() {
        when:
        getGradleRunner(true).build()

        then:
        new File(buildDir, 'docs/html').exists()
        new File(buildDir, 'docs/html/index.html').exists()
    }

    void "When 'template' is supplied as a string, configuration should still work"() {
        setup: 'A task configured with a custom template which is supplied as a string'
        buildFile << """
        doxygen {
            sourceSets {
              main {
                template '${DOXY_TEMPLATE.absolutePath.replace('\\', '\\\\')}'
              }
            }
        }
        """.stripIndent()

        when: 'The task is executed'
        gradleRunner.build()
        def lines = new File(buildDir, 'tmp/doxygen/doxygenDox.doxyfile').text.readLines()

        then: 'The HTML files should have been created'
        new File(buildDir, 'docs/html').exists()
        new File(buildDir, 'docs/html/index.html').exists()

        and: 'Lines from the custom template should have been used'
        lines.find { 'FILE_PATTERNS =' }
    }

    private GradleRunner getGradleRunner(boolean withConfigCache = false) {

        final args = [
            'doxygen', '-i', '-s',
            "-Porg.ysb33r.gradle.doxygen.download.url=${DOWNLOAD_CACHE_DIR.toURI()}".toString()
        ]

        if (withConfigCache) {
            args.addAll(['--configuration-cache', '--configuration-cache-problems=fail'])
        }

        GradleRunner.create()
            .withProjectDir(projectDir)
            .withTestKitDir(testkitDir)
            .withArguments(args)
            .withPluginClasspath()
            .forwardOutput()
            .withDebug(true)
    }

}