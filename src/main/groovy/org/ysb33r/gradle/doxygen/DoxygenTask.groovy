/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.doxygen

import groovy.transform.CompileStatic
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.file.FileCollection
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import org.ysb33r.gradle.doxygen.internal.DoxyfileEditor
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask

/**
 * Runs the {@code doxygen} executable to generate documentation.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class DoxygenTask extends GrolifantDefaultTask {

    /**
     * Constructs a Doxygen task object and sets some default Doxygen properties
     */
    DoxygenTask() {
        this.doxyTempFile = fsOperations().buildDirDescendant("tmp/doxygen/${name}.doxyfile")
        this.outputDir = project.objects.property(File)
        this.templateFile = project.objects.property(File)
        this.imagePaths = fsOperations().emptyFileCollection()
        this.options = project.objects.mapProperty(String, String)
        this.doxygenPath = project.objects.property(File)
        this.workingDir = project.projectDir
    }

    /**
     * Documentation output directory
     *
     * @return Directory that was set earlier via {@link #setOutputDir} otherwise {@code $buildDir/docs/doxygen}   .
     */
    @OutputDirectory
    Provider<File> getOutputDir() {
        this.outputDir
    }

    /** Sets the output directory
     *
     * @param dir A provider to a directory.
     */
    void setOutputDir(Provider<File> dir) {
        this.outputDir.set(dir)
    }

    @Optional
    @InputFile
    Provider<File> getTemplate() {
        this.templateFile
    }

    /** Sets the template Doxyfile to be used. If not supplied a default one will be
     * generated to be used as a template.
     *
     * @param tmpl Template Doxyfile
     */
    void setTemplate(Provider<File> tmpl) {
        templateFile.set(tmpl)
    }

    @InputFiles
    FileCollection getImagePaths() {
        this.imagePaths
    }

    void setImagePaths(FileCollection paths) {
        this.imagePaths.from = paths
    }

    void setDoxygenProperties(Provider<Map<String, String>> opts) {
        this.options.set(opts)
    }

    /**
     * Returns the current hashmap of Doxygen properties that will override settings in the Doxygen file
     */
    @Input
    Provider<Map<String, String>> getDoxygenProperties() {
        this.options
    }

    @Internal
    Provider<File> getDoxygenLocation() {
        this.doxygenPath
    }

    void setDoxygenLocation(Provider<File> exe) {
        this.doxygenPath.set(exe)
    }

    @TaskAction
    void exec() {
        final dox = doxygenLocation.get()
        File doxyfile = createDoxyfile(dox)
        logger.debug "Using ${doxyfile} as Doxygen configuration file"
        editDoxyfile(doxyfile)
        runDoxygen(dox, workingDir, [doxyfile.absolutePath])
    }

    private File createDoxyfile(File doxPath) {
        File doxyfile = doxyTempFile.get()
        File useTemplate = templateFile.orNull
        doxyfile.parentFile.mkdirs()
        if (useTemplate) {
            if (!useTemplate.exists()) {
                throw new DoxygenException("${useTemplate} does not exist")
            }
            doxyfile.text = useTemplate.text
        } else {
            runDoxygen(doxPath, doxyfile.parentFile, ['-g', doxyfile.name])
        }

        doxyfile
    }

    private void editDoxyfile(File doxyfile) {
        DoxyfileEditor editor = new DoxyfileEditor()
        editor.update(options.get(), doxyfile)
    }

    private void runDoxygen(final File doxPath, File doxWorkdir, Collection<String> cmdargs = []) {
        execTools().exec { spec ->
            spec.tap {
                executable = doxPath
                workingDir = doxWorkdir
                args(cmdargs)
            }
        }
    }

    private final Property<File> templateFile
    private final Property<File> outputDir
    private final ConfigurableFileCollection imagePaths
    private final MapProperty<String, String> options
    private final Property<File> doxygenPath
    private final Provider<File> doxyTempFile
    private final File workingDir
}

