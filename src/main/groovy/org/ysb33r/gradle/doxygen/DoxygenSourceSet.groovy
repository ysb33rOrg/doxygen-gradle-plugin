/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.doxygen

import groovy.transform.CompileStatic
import org.gradle.api.Named
import org.gradle.api.Project
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.file.FileCollection
import org.gradle.api.file.FileTree
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.util.PatternFilterable
import org.gradle.api.tasks.util.PatternSet
import org.ysb33r.gradle.doxygen.internal.DoxygenProperties
import org.ysb33r.gradle.doxygen.internal.DoxygenUtils
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.internal.core.IterableUtils

import javax.inject.Inject
import java.util.regex.Pattern

import static org.ysb33r.gradle.doxygen.internal.DoxygenProperties.escapeAndJoinFileCollection
import static org.ysb33r.gradle.doxygen.internal.DoxygenProperties.escapeValue

/**
 * Describes a set of sources
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DoxygenSourceSet implements Named, PatternFilterable {

    final String name

    @Inject
    @SuppressWarnings('AbcMetric')
    DoxygenSourceSet(String name, DoxygenExtension parent, Project project) {
        this.name = name
        this.ccso = ConfigCacheSafeOperations.from(project)
        this.defaultSourceDir = project.objects.property(File)
        this.templateFile = project.objects.property(File)
        this.imagePaths = ccso.fsOperations().emptyFileCollection()
        this.sources = ccso.fsOperations().emptyFileCollection()
        this.patternSet = new PatternSet()
        this.visibleSources = this.sources.asFileTree.matching(this.patternSet)
        this.doxyUpdate = new DoxygenProperties(ccso)
        this.outputDir = project.objects.property(File)

        this.outputDir.set(ccso.fsOperations().buildDirDescendant("docs/${DoxygenUtils.outputSubdir(name)}"))
        ccso.fsOperations().updateFileProperty(defaultSourceDir, DoxygenUtils.srcSubdir(name))
        this.sources.from(this.defaultSourceDir)

        doxyUpdate.option('PROJECT_NAME', ccso.projectTools().projectNameProvider)
        doxyUpdate.option('QUIET', project.logger.quietEnabled)
        doxyUpdate.option('WARNINGS', !project.logger.quietEnabled)
        doxyUpdate.option('PROJECT_NUMBER', ccso.projectTools().versionProvider)

        this.finalDoxygenProperties = doxyUpdate.options.zip(parent.otherExecutables) { doxyOpts, exes ->
            Map<String,String> opts = [:]
            opts.putAll(doxyOpts)

            if (owner.imagePaths.size()) {
                opts.put('IMAGE_PATH', escapeAndJoinFileCollection(getImagePaths()))
            }

            opts.put('INPUT', escapeAndJoinFileCollection(owner.visibleSources))
            opts.put('OUTPUT_DIRECTORY', escapeValue(owner.outputDir.get().absolutePath))
            exes.each { doxProp, location ->
                opts.put(doxProp, escapeValue(location))
            }

            (Map)opts
        }
    }

    Provider<File> getDefaultSourceDir() {
        this.defaultSourceDir
    }

    /**
     * The directory where template files can be written to and where most non-code-based Doxygen source files can be
     * found in.
     *
     * @param file Anything convertible to a file.
     */
    void setDefaultSourceDir(Object file) {
        ccso.fsOperations().updateFileProperty(this.defaultSourceDir, file)
    }

    /**
     *  Documentation output directory
     *
     * @return Directory that was set earlier via {@link #setOutputDir} otherwise {@code $buildDir/docs/doxygen}   .
     */
    Provider<File> getOutputDir() {
        this.outputDir
    }

    /** Sets the output directory
     *
     * @param dir A path that can be converted with {@code project.file}.
     */
    void setOutputDir(Object dir) {
        ccso.fsOperations().updateFileProperty(this.outputDir, dir)
    }

    Provider<File> getTemplate() {
        this.templateFile
    }

    /** Sets the template Doxyfile to be used. If not supplied a default one will be
     * generated to be used as a template.
     *
     * @param tmpl Template Doxyfile
     */
    void setTemplate(final File tmpl) {
        this.templateFile.set(tmpl)
    }

    /** Sets the template Doxyfile to be used. If not supplied a default one will be
     * generated to be used as a template.
     *
     * @param tmpl Template Doxyfile
     */
    void setTemplate(final String tmpl) {
        this.templateFile.set(ccso.fsOperations().provideFile(tmpl))
    }

    ConfigurableFileCollection getImagePaths() {
        this.imagePaths
    }

    /**
     * Location of images.
     *
     * @param paths Anything convertible to a file.
     */
    void imagePaths(Object... paths) {
        paths.each {
            this.imagePaths.from(ccso.fsOperations().provideFile(it))
        }
    }

    FileCollection getSources() {
        this.visibleSources
    }

    void setSources(Object src) {
        this.sources.from = ccso.fsOperations().emptyFileCollection().from(src)
    }

    void sources(Object... src) {
        this.sources.from(src)
    }

    /**
     * Provides a single Doxygen property.
     *
     * @param key Name opf Doxygen property
     * @param value Value of property
     */
    void option(String key, Object value) {
        switch (key) {
            case 'aliases':
            case 'allexternals':
            case 'exclude':
            case 'predefined':
            case 'quiet':
            case 'recursive':
            case 'searchengine':
            case 'subgrouping':
            case 'warnings':
                doxyUpdate.option(key, value)
                break

            case 'input':
                throw new DoxygenException("'${key}' is ignored, use 'source' and 'sourceDir' instead (with exclude patterns as appropriate).")

            case 'mscgen_path':
            case 'dot_path':
            case 'perl_path':
            case 'hhc_location':
                throw new DoxygenException("'${key}' is ignored, use 'executables' instead.")

            default:
                if (key.find(/.+_.+/) && VALID_PROPERTY_NAME.matcher(key).matches()) {
                    if (value instanceof Collection) {
                        doxyUpdate.listOption(key, (Collection) value)
                    } else if (IterableUtils.treatAsIterable(value)) {
                        doxyUpdate.listOption(key, (Iterable) value)
                    } else {
                        doxyUpdate.option(key, value)
                    }
                } else {
                    throw new DoxygenException("'${key}' is not a valid configuration option")
                }
        }
    }

    /**
     * Provide many Doxygen properties via a map.
     *
     * @param settings Map of settings
     */
    void options(Map<String, Object> settings) {
        settings.each { key, value ->
            option(key, value)
        }
    }

    /**
     * Returns the current hashmap of Doxygen properties that will override settings in the Doxygen file
     */
    Provider<Map<String, String>> getDoxygenProperties() {
        this.finalDoxygenProperties
    }

    private final ConfigCacheSafeOperations ccso
    private final Property<File> defaultSourceDir
    private final Property<File> templateFile
    private final Property<File> outputDir
    private final ConfigurableFileCollection imagePaths
    private final DoxygenProperties doxyUpdate
    private final ConfigurableFileCollection sources
    private final FileTree visibleSources
    private final Provider<Map<String,String>> finalDoxygenProperties

    @Delegate
    private final PatternSet patternSet

    private static final Pattern VALID_PROPERTY_NAME = ~/[_\p{Digit}\p{Lower}]{3,}/
}
