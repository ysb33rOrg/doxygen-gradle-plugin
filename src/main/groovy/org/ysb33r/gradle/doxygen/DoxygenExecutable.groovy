/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.doxygen

import groovy.transform.CompileStatic
import org.ysb33r.gradle.doxygen.internal.DoxygenDistributionDownloader
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.downloader.ExecutableDownloader
import org.ysb33r.grolifant5.api.core.runnable.AbstractToolExtension
import org.ysb33r.grolifant5.api.core.runnable.ToolLocation
import org.ysb33r.grolifant5.api.errors.ConfigurationException

/**
 * Managing the location and download of the {@code doxygen} executable.
 */
@CompileStatic
class DoxygenExecutable extends AbstractToolExtension<DoxygenExecutable> implements ToolLocation {
    DoxygenExecutable(ProjectOperations projectOperations) {
        super(projectOperations)
        this.downloader = new DoxygenDistributionDownloader(projectOperations)
    }

    @Override
    @SuppressWarnings('CatchRuntimeException')
    protected String runExecutableAndReturnVersion() throws ConfigurationException {
        File exec = executablePathOrNull() ?: executableSearchPathOrNull()
        if (!exec) {
            return null
        }
        try {
            projectOperations.execTools.parseVersionFromOutput(['-v'], exec) { String output ->
                output.readLines()[0]
            }
        } catch (RuntimeException e) {
            throw new ConfigurationException('Cannot determine version', e)
        }
    }

    @Override
    protected ExecutableDownloader getDownloader() {
        this.downloader
    }

    private final DoxygenDistributionDownloader downloader
}
