/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.doxygen.internal

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.doxygen.DotExecutable
import org.ysb33r.gradle.doxygen.DoxygenExecutable
import org.ysb33r.gradle.doxygen.Executables
import org.ysb33r.gradle.doxygen.MscgenExecutable
import org.ysb33r.gradle.doxygen.OptionalBaseToolLocation
import org.ysb33r.gradle.doxygen.OptionalToolLocation
import org.ysb33r.gradle.doxygen.SystemExecutable
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.runnable.ToolLocation

import static org.ysb33r.grolifant5.api.core.ClosureUtils.configureItem
import static org.ysb33r.grolifant5.api.core.ResourceUtils.loadPropertiesFromResource

/**
 *
 * Implementation for configuring executables.
 *
 * @since 1.0
 */
@CompileStatic
class ExecutablesImpl implements Executables {

    @SuppressWarnings('AbcMetric')
    ExecutablesImpl(Project project) {
        Properties props = loadPropertiesFromResource('META-INF/doxygen-gradle/doxygen.properties')
        this.projectOperations = ProjectOperations.find(project)

        this.doxygenExecutable = new DoxygenExecutable(this.projectOperations)
        if (DoxygenDistributionDownloader.DOWNLOAD_SUPPORTED) {
            this.doxygenExecutable.executableByVersion(props['doxygen'])
        }

        this.dotExecutable = new DotExecutable(this.projectOperations)
        if (DotDistributionDownloader.DOWNLOAD_SUPPORTED) {
            this.dotExecutable.executableByVersion(props['graphviz'])
        }

        this.mscgenExecutable = new MscgenExecutable(this.projectOperations)
        if (MscgenDistributionDownloader.DOWNLOAD_SUPPORTED) {
            this.mscgenExecutable.executableByVersion(props['mscgen'])
        }

        this.perlExecutable = new SystemExecutable(this.projectOperations)
        this.perlExecutable.executableBySearchPath('perl')

        this.hhcExecutable = new SystemExecutable(this.projectOperations)
        this.hhcExecutable.executableBySearchPath('chm')

        this.otherExecutableLocations = project.provider { ->
            final exes = new TreeMap<String, String>()

            if (owner.dotExecutable.enabled) {
                exes.put('DOT_PATH', owner.dotExecutable.executable.get().absolutePath)
            }
            if (owner.mscgenExecutable.enabled) {
                exes.put('MSCGEN_PATH', owner.mscgenExecutable.executable.get().absolutePath)
            }
            if (owner.perlExecutable.enabled) {
                exes.put('PERL_PATH', owner.perlExecutable.executable.get().absolutePath)
            }
            if (owner.hhcExecutable.enabled) {
                exes.put('HHC_LOCATION', owner.hhcExecutable.executable.get().absolutePath)
            }
            exes as Map
        }
    }

    @Override
    void doxygen(Action<ToolLocation> configurator) {
        configurator.execute(this.doxygenExecutable)
    }

    @Override
    void doxygen(@DelegatesTo(ToolLocation) Closure configurator) {
        configureItem(this.doxygenExecutable, configurator)
    }

    DoxygenExecutable getDoxygen() {
        this.doxygenExecutable
    }

    @Override
    void dot(Action<OptionalToolLocation> configurator) {
        configurator.execute(this.dotExecutable)
    }

    @Override
    void dot(@DelegatesTo(OptionalToolLocation) Closure configurator) {
        configureItem(this.dotExecutable, configurator)
    }

    @Override
    void hhc(Action<OptionalBaseToolLocation> configurator) {
        configurator.execute(this.hhcExecutable)
    }

    @Override
    void hhc(@DelegatesTo(OptionalBaseToolLocation) Closure configurator) {
        configureItem(this.hhcExecutable, configurator)
    }

    @Override
    void mscgen(Action<OptionalToolLocation> configurator) {
        configurator.execute(this.mscgenExecutable)
    }

    @Override
    void mscgen(@DelegatesTo(OptionalToolLocation) Closure configurator) {
        configureItem(this.mscgenExecutable, configurator)
    }

    @Override
    void perl(Action<OptionalBaseToolLocation> configurator) {
        configurator.execute(this.perlExecutable)
    }

    @Override
    void perl(@DelegatesTo(OptionalBaseToolLocation) Closure configurator) {
        configureItem(this.perlExecutable, configurator)
    }

    Provider<Map<String, String>> getOtherExecutables() {
        this.otherExecutableLocations
    }

    private final ProjectOperations projectOperations
    private final DoxygenExecutable doxygenExecutable
    private final DotExecutable dotExecutable
    private final MscgenExecutable mscgenExecutable
    private final SystemExecutable perlExecutable
    private final SystemExecutable hhcExecutable
    private final Provider<Map<String, String>> otherExecutableLocations
}
