/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.doxygen.internal

import groovy.transform.CompileStatic
import org.gradle.api.NamedDomainObjectFactory
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.ysb33r.gradle.doxygen.DoxygenExtension
import org.ysb33r.gradle.doxygen.DoxygenSourceSet

/**
 * A factory for {@link DoxygenSourceSet} instances.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DoxygenSourceSetFactory implements NamedDomainObjectFactory<DoxygenSourceSet> {

    DoxygenSourceSetFactory(Project project, DoxygenExtension parent) {
        this.objectFactory = project.objects
        this.parent = parent
    }

    @Override
    DoxygenSourceSet create(String name) {
        objectFactory.newInstance(DoxygenSourceSet, name, parent)
    }

    private final DoxygenExtension parent
    private final ObjectFactory objectFactory
}
