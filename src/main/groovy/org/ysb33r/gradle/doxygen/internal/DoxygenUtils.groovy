/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.doxygen.internal

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.doxygen.DoxygenExtension
import org.ysb33r.gradle.doxygen.DoxygenSourceSet
import org.ysb33r.gradle.doxygen.DoxygenTask
import org.ysb33r.gradle.doxygen.DoxygenTemplateFiles

import static org.ysb33r.gradle.doxygen.DoxygenExtension.MAIN_SOURCESET_NAME
import static org.ysb33r.gradle.doxygen.DoxygenPlugin.DOX_GROUP

/**
 * Utilities to deal with source sets.
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DoxygenUtils {
    static String srcSubdir(String sourceSetName) {
        "src/${sourceSetName}/doxygen"
    }

    static String outputSubdir(String sourceSetName) {
        sourceSetName == MAIN_SOURCESET_NAME ? 'doxygen' : "doxygen${sourceSetName.capitalize()}"
    }

    static void createTasksFromModel(DoxygenSourceSet dss, Project project) {
        final doxygen = project.extensions.getByType(DoxygenExtension)
        project.tasks.register(doxygenTaskName(dss), DoxygenTask) { t ->
            t.tap {
                group = DOX_GROUP
                description = 'Runs doxygen'
                outputDir = dss.outputDir
                template = dss.template
                imagePaths = dss.imagePaths
                doxygenProperties = dss.doxygenProperties
                doxygenLocation = doxygen.doxygenExecutable
            }
        }

        project.tasks.register(doxygenTemplatesTaskName(dss), DoxygenTemplateFiles) { t ->
            t.tap {
                group = DOX_GROUP
                description = 'Creates templates for Doxygen'
                outputDir = dss.defaultSourceDir
                prefix = project.provider { -> 'dox' }
            }
        }
    }

    static String doxygenTaskName(DoxygenSourceSet dss) {
        dss.name == MAIN_SOURCESET_NAME ? 'doxygenDox' : "doxygen${dss.name.capitalize()}Dox"
    }

    static String doxygenTemplatesTaskName(DoxygenSourceSet dss) {
        dss.name == MAIN_SOURCESET_NAME ? 'createDoxygenTemplates' : "createDoxygen${dss.name.capitalize()}Templates"
    }
}
