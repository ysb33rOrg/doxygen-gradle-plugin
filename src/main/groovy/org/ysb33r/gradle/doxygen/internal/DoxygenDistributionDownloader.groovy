/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.doxygen.internal

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.Version
import org.ysb33r.grolifant5.api.errors.DistributionFailedException

/**
 * Downloads specific versions of Doxygen.
 * Currently limited to Linux, Windows & MacOS X on x86 32 + 64-bit architectures as these are the only ones for which
 * binary packages are available from the Doxygen site.
 */
@CompileStatic
class DoxygenDistributionDownloader extends BaseDistributionInstaller {
    public static final OperatingSystem OS = OperatingSystem.current()
    public static final boolean DOWNLOAD_SUPPORTED = OS.linux || OS.macOsX || OS.windows

    DoxygenDistributionDownloader(final ProjectOperations projectOperations) {
        super(
            DOXYGEN,
            'native-binaries/doxygen',
            'https://github.com/doxygen/doxygen/releases/download/',
            DOXYGEN,
            ConfigCacheSafeOperations.from(projectOperations)
        )
        this.legacyBaseURI = projectOperations.providerTools.resolveProperty(
            'org.ysb33r.gradle.doxygen.legacy.download.url',
            'https://downloads.sourceforge.net/project/doxygen'
        )
    }

    /** Provides an appropriate URI to download a specific verson of Doxygen.
     *
     * @param ver Version of Doxygen to download
     * @return URI for Linux, Windows (32,64 bit) or MacOSX. {@code null} otherwise
     */
    @Override
    URI uriFromVersion(final String ver) {
        final version = Version.of(ver)
        final legacyDownload = version.major < 1 || version.major == 1 && version.minor < 9
        // Versions older than 1.9 looks like this
        //   https://sourceforge.net/projects/doxygen/files/rel-1.8.20/doxygen-1.8.20.windows.x64.bin.zip
        // whereas the newer versions look like this
        //   https://github.com/doxygen/doxygen/releases/download/Release_1_9_2/doxygen-1.9.2.windows.x64.bin.zip
        final String base = legacyDownload ?
            "${legacyBaseURI.get()}/rel-${ver}" :
            "${baseURI.get()}/Release_${ver.replaceAll(~/\./, '_')}"

        if (OS.windows) {
            if (legacyDownload) {
                if (version.major < 1 || version.major == 1 && version.minor < 8) {
                    "${base}/doxygen-${ver}.windows.bin.zip".toURI()
                } else {
                    if (OS.arch == OperatingSystem.Arch.X86_64) {
                        "${base}/doxygen-${ver}.windows.x64.bin.zip".toURI()
                    } else {
                        "${base}/doxygen-${ver}.windows.x64.bin.zip".toURI()
                    }
                }
            } else {
                "${base}/doxygen-${ver}.windows.x64.bin.zip".toURI()
            }
        } else if (OS.linux) {
            "${base}/doxygen-${ver}.linux.bin.tar.gz".toURI()
        } else if (OS.macOsX) {
            "${base}/Doxygen-${ver}.dmg".toURI()
        } else {
            null
        }
    }

    @Override
    File getByVersion(String version) {
        getDoxygenExecutablePath(version).get()
    }

    /** Adds a special verification case for Doxygen Windows binaries.
     *
     * The Windows distribution is not zipped into a subfolder and needs extra care.
     * For Linux & MacOsX it will use the default implementation.
     *
     * @param distDir Directory where distribution was unpacked to.
     * @return Distribution directory
     */
    @Override
    protected File verifyDistributionRoot(File distDir) {
        if (OS.windows) {
            if (!new File(distDir, "${DOXYGEN}.exe").exists()) {
                throw new DistributionFailedException("Doxygen does not contain '${DOXYGEN}.exe'.")
            }
            distDir
        } else {
            super.verifyDistributionRoot(distDir)
        }
    }

    /** Returns the path to the Doxygen executable.
     * Will force a download if not already downloaded.
     *
     * @return Location of {@code doxygen} or null if not a supported operating system.
     */
    Provider<File> getDoxygenExecutablePath(String version) {
        if (OS.windows) {
            getDistributionFile(version, "${DOXYGEN}.exe")
        } else if (OS.linux) {
            getDistributionFile(version, "bin/${DOXYGEN}")
        } else if (OS.macOsX) {
            getDistributionFile(version, "Contents/Resources/${DOXYGEN}")
        } else {
            null
        }
    }

    private final Provider<String> legacyBaseURI

    private static final String DOXYGEN = 'doxygen'
}

