/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.doxygen

import groovy.transform.CompileStatic
import org.gradle.api.tasks.Internal
import org.ysb33r.gradle.doxygen.internal.DotDistributionDownloader
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.downloader.ExecutableDownloader
import org.ysb33r.grolifant5.api.core.runnable.AbstractToolExtension
import org.ysb33r.grolifant5.api.errors.ConfigurationException

/**
 * Location of {@code dot} executable from Graphviz.
 *
 * Default behaviour is to down a Graphviz package on Windows, but used the installed version on other platforms.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.0
 */
@CompileStatic
class DotExecutable extends AbstractToolExtension<DotExecutable> implements OptionalToolLocation {
    DotExecutable(ProjectOperations projectOperations) {
        super(projectOperations)
        this.downloader = new DotDistributionDownloader(projectOperations)
    }

    @Internal
    boolean enabled = false

    @Override
    @SuppressWarnings('CatchRuntimeException')
    protected String runExecutableAndReturnVersion() throws ConfigurationException {
        File exec = executablePathOrNull() ?: executableSearchPathOrNull()
        if (!exec) {
            return null
        }
        try {
            projectOperations.execTools.parseVersionFromOutput(['-V'], exec) { String output ->
                final versionLine = output.readLines()[0]
                versionLine.split(output.readLines()[0])[4]
            }
        } catch (RuntimeException e) {
            throw new ConfigurationException('Cannot determine version', e)
        }
    }

    @Override
    protected ExecutableDownloader getDownloader() {
        this.downloader
    }

    private final DotDistributionDownloader downloader
}
