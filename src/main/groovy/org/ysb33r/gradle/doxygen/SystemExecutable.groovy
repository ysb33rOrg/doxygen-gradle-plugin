/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.doxygen

import groovy.transform.CompileStatic
import org.gradle.api.tasks.Internal
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.runnable.AbstractBaseToolExtension
import org.ysb33r.grolifant5.api.errors.ConfigurationException

/**
 * Obtains a path to a system-installed executable.
 */
@CompileStatic
class SystemExecutable extends AbstractBaseToolExtension<SystemExecutable> implements OptionalBaseToolLocation {

    @Internal
    boolean enabled = false

    SystemExecutable(ProjectOperations projectOperations) {
        super(projectOperations)
    }

    @Override
    protected String runExecutableAndReturnVersion() throws ConfigurationException {
        return null
    }
}
