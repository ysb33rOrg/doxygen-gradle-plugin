/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.doxygen;

import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.gradle.api.Action;
import org.ysb33r.grolifant5.api.core.runnable.ToolLocation;

/**
 * Configure exutable locaitons in the Doxygen universe.
 */
public interface Executables {
    /**
     * Configures the Graphviz dot executable.
     *
     * @param configurator Configurator.
     */
     void dot(Action<OptionalToolLocation> configurator);

     void dot(@DelegatesTo(OptionalToolLocation.class) Closure configurator);

    /**
     * Configures the Doxygen executable.
     *
     * @param configurator Configurator.
     */
     void doxygen(Action<ToolLocation> configurator);

     void doxygen(@DelegatesTo(ToolLocation.class) Closure configurator);

    /**
     * Configures Windows Help Compiler.
     *
     * @param configurator Configurator.
     */
     void hhc(Action<OptionalBaseToolLocation> configurator);

     void hhc(@DelegatesTo(OptionalBaseToolLocation.class) Closure configurator);

    /**
     * Configures the Mscgen executable.
     *
     * @param configurator Configurator.
     */
     void mscgen(Action<OptionalToolLocation> configurator);

     void mscgen(@DelegatesTo(OptionalToolLocation.class) Closure configurator);

    /**
     * Configures Perl.
     *
     * @param configurator Configurator.
     */
     void perl(Action<OptionalBaseToolLocation> configurator);

     void perl(@DelegatesTo(OptionalBaseToolLocation.class) Closure configurator);
}
