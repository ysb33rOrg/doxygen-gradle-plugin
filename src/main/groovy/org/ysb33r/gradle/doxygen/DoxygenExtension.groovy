/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.doxygen

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.doxygen.internal.DoxygenSourceSetFactory
import org.ysb33r.gradle.doxygen.internal.DoxygenUtils
import org.ysb33r.gradle.doxygen.internal.ExecutablesImpl
import org.ysb33r.grolifant5.api.core.ClosureUtils

/**
 * Defines Doxygen source sets
 *
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DoxygenExtension {
    public static final String NAME = 'doxygen'
    public static final String MAIN_SOURCESET_NAME = 'main'

    final NamedDomainObjectContainer<DoxygenSourceSet> sourceSets

    DoxygenExtension(Project project) {
        this.executableLocations = new ExecutablesImpl(project)

        this.sourceSetFactory = new DoxygenSourceSetFactory(project, this)
        this.sourceSets = project.objects.domainObjectContainer(DoxygenSourceSet, this.sourceSetFactory)

        this.sourceSets.whenObjectAdded { DoxygenSourceSet dss ->
            DoxygenUtils.createTasksFromModel(dss, project)
        }
    }

    /**
     * Allows for setting paths to various executables.
     *
     * <p>
     * By default specific versions of {@code doxygen} and {@code dot} will be downloaded.
     * {@code mscgen}
     * </p>
     *
     * @param cfg A configuration closure
     *
     */
    void executables(Action<Executables> cfg) {
        cfg.execute(this.executableLocations)
    }

    /**
     * Allows for setting paths to various executables.
     *
     * <p>
     * By default specific versions of {@code doxygen} and {@code dot} will be downloaded.
     * {@code mscgen}
     * </p>
     *
     * @param cfg A configuration closure
     *
     */
    void executables(@DelegatesTo(Executables) Closure cfg) {
        ClosureUtils.configureItem(this.executableLocations, cfg)
    }

    Provider<File> getDoxygenExecutable() {
        executableLocations.doxygen.executable
    }

    Provider<Map<String, String>> getOtherExecutables() {
        executableLocations.otherExecutables
    }

    private final ExecutablesImpl executableLocations
    private final DoxygenSourceSetFactory sourceSetFactory
}
