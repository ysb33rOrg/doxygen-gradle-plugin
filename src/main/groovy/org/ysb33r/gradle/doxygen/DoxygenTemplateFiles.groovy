/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.doxygen

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.TaskAction
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask

import javax.inject.Inject

/**
 * Allows for the creation of template files in a location. 'src/doxygen' is the default.
 */
@CompileStatic
class DoxygenTemplateFiles extends GrolifantDefaultTask {

    @Inject
    DoxygenTemplateFiles() {
        this.outputDirectory = project.objects.property(File)
        this.filePrefix = project.objects.property(String)
        this.doxygenLocation = project.objects.property(File)
    }

    /**
     * Location where to generate files into. By default this will be src/main/doxygen.
     *
     * @param dir Anything convertible to a {@code File}.
     */
    void setOutputDir(Provider<File> file) {
        this.outputDirectory.set(file)
    }

    /**
     * Prefix used for naming files. By default it is the name of the project.
     *
     * @param prefix project prefix
     */
    void setPrefix(Provider<String> prefix) {
        this.filePrefix.set(prefix)
    }

    @SuppressWarnings('DuplicateStringLiteral')
    @TaskAction
    void exec() {
        final File location = outputDirectory.get()
        final prefix = filePrefix.get()
        location.mkdirs()
        runDoxygen('-l', new File(location, prefix + 'LayoutTemplate.xml').absolutePath)
        runDoxygen('-w', 'rtf', new File(location, prefix + 'Style.rtf').absolutePath)
        runDoxygen('-w', 'html',
            new File(location, prefix + 'Header.html').absolutePath,
            new File(location, prefix + 'Footer.html').absolutePath,
            new File(location, prefix + '.css').absolutePath
        )
        runDoxygen('-w', 'latex',
            new File(location, prefix + 'Header.tex').absolutePath,
            new File(location, prefix + 'Footer.tex').absolutePath,
            new File(location, prefix + 'Style.tex').absolutePath
        )
        runDoxygen('-e', 'rtf', new File(location, prefix + 'Extensions.rtf').absolutePath)
    }

    /** Runs the Doxygen executable
     *
     * @param cmdargs
     */
    private void runDoxygen(String... cmdargs) {
        execTools().exec { spec ->
            spec.executable = doxygenLocation.get()
            spec.args(cmdargs)
        }
    }

    private final Property<String> filePrefix
    private final Property<File> outputDirectory
    private final Provider<File> doxygenLocation
}
