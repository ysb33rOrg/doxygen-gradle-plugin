/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.doxygen

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.language.base.plugins.LifecycleBasePlugin
import org.ysb33r.grolifant5.api.core.plugins.GrolifantServicePlugin

/**
 * @author Schalk W. Cronjé
 *
 * @since 2.0
 */
@CompileStatic
class DoxygenBasePlugin implements Plugin<Project> {
    public static final String DOX_TASK_NAME = 'doxygen'
    public static final String DOX_TEMPLATE_TASK_NAME = 'createDoxygenTemplates'
    public static final String DOX_GROUP = 'Documentation'

    void apply(Project project) {

        project.pluginManager.tap {
            apply(LifecycleBasePlugin)
            apply(GrolifantServicePlugin)
        }

        project.extensions.create(DoxygenExtension.NAME, DoxygenExtension, project)
    }
}